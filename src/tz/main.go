package main

import (
	"fmt"
	"tz/app/config/routes"
	"net/http"
)

func main() {
	//для удобства выносим основные параметры для запуска сервера
	ipServer, portServer := "127.0.0.1", "8000"

	//выводим на экран о том, что сервер запущен
	fmt.Println("START SERVER " + ipServer + ":" + portServer)

	//подключаем пакет маршрутизации
	routes.Routes()

	//ставим на прослушку серве
	http.ListenAndServe(ipServer + ":" + portServer, nil)
}
