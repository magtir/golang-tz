package MainController


import (
	"html/template"
	"fmt"
	"net/http"
	"tz/app/Modules/Main/Models/ScoreModel"
)

func Index(w http.ResponseWriter, r *http.Request) {
	//стандартный пакет, выводим шаблон
	t, err := template.ParseFiles("app/templates/pages/home/index.html")
	if err != nil {
		fmt.Println("#ERR->MainController->Index", err)
		return
	}

	t.ExecuteTemplate(w, "index.html", nil)
}

func Show(w http.ResponseWriter, r *http.Request) {
	var response int

	//если GET запрос, то берем переменную Score
	//В противном случае в шаблон придет ноль и в нем сработает условие на показ ошибки
	if r.Method == http.MethodGet {
		response = ScoreModel.Score
	}
	t, err := template.ParseFiles("app/templates/pages/show/index.html")
	if err != nil {
		fmt.Println("#ERR->MainController->Show", err)
		return
	}

	t.ExecuteTemplate(w, "index.html", response)
}

func Add(w http.ResponseWriter, r *http.Request) {

	//Если Пост запрос был
	if r.Method == http.MethodPost {

		//запускаем горутину в которой заполняем канал
		go func() {
			ScoreModel.ChanScore <- 1
		}()
		//Читаем канал и сразу инкрементим переменную Score
		ScoreModel.Score += <- ScoreModel.ChanScore

		//делаем редирект
		http.Redirect(w, r, "/show", http.StatusSeeOther)
	}

	//Если поста не было, выводим шаблон для изменения счетчика
	t, err := template.ParseFiles("app/templates/pages/add/index.html")
	if err != nil {
		fmt.Println("#ERR->MainController->Add", err)
		return
	}

	t.ExecuteTemplate(w, "index.html", nil)
}