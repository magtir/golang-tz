package routes

import (
	"net/http"
	"tz/app/Modules/Main/Controllers/MainController"
)

func Routes() {
	//роуты
	http.HandleFunc("/", MainController.Index)
	http.HandleFunc("/show", MainController.Show)
	http.HandleFunc("/add", MainController.Add)
}
